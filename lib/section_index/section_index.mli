(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

val default : string

val valid : string
val valid_long : string

val select : string -> (module Index_module.t_index)

type t_print_end_total_fn = int -> unit
val select_print_end_total : bool -> t_print_end_total_fn
