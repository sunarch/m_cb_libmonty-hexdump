(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

type r_index = {

    short : string;
    medium : string;
    long : string;

    valid: string;

    prefix : string; prefix_empty: string;
    unit_width : int;
    number_format : Number_format.t;
    joiner : string;

    t_threshold: int;
    g_threshold: int; g_extra: string; g_empty: string;
    m_threshold: int; m_extra: string; m_empty: string;
    k_threshold: int; k_extra: string; k_empty: string;
    b_threshold: int; b_extra: string; b_empty: string;
}

val init_empty_prefix : string -> string
val init_empty_g : r_index -> string
val init_empty_m : r_index -> string
val init_empty_k : r_index -> string
val init_empty_b : r_index -> string

val number_formatter_regular : int -> Number_format.t -> (int -> string)
val number_formatter_highest : Number_format.t -> (int -> string)
val unit_formatter : (int -> string) -> string -> string -> int -> string

type t_total_fn_with_args = int -> string
val total : r_index
    -> (int -> string) -> (int -> string) -> (int -> string) -> (int -> string)
    -> t_total_fn_with_args
val empty : r_index -> string

type t_print_fn_with_args = int -> unit

type t_print_select_fn = bool -> string -> t_print_fn_with_args
val select_print : t_total_fn_with_args -> t_print_select_fn

val print_empty : string -> unit -> unit
