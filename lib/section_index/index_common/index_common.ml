(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

type r_index = {

    short : string;
    medium : string;
    long : string;

    valid: string;

    prefix : string; prefix_empty: string;
    unit_width : int;
    number_format : Number_format.t;
    joiner : string;

    t_threshold: int;
    g_threshold: int; g_extra: string; g_empty: string;
    m_threshold: int; m_extra: string; m_empty: string;
    k_threshold: int; k_extra: string; k_empty: string;
    b_threshold: int; b_extra: string; b_empty: string;
}

let init_empty_prefix (prefix: string) : string =
    let (prefix_width: int) = String.length prefix in
    String.make prefix_width ' '

let init_empty_general
        (unit_width: int)
        (extra: string)
        (joiner: string)
        : string =

    let (extra_width: int) = String.length extra in
    let (joiner_width: int) = String.length joiner in
    String.make (unit_width + extra_width + joiner_width) ' '

let init_empty_g (record: r_index) : string =
    init_empty_general 1 record.g_extra record.joiner

let init_empty_m (record: r_index) : string =
    init_empty_general record.unit_width record.m_extra record.joiner

let init_empty_k (record: r_index) : string =
    init_empty_general record.unit_width record.k_extra record.joiner

let init_empty_b (record: r_index) : string =
    init_empty_general record.unit_width record.b_extra ""

let number_formatter_regular
        (unit_width: int)
        (number_format: Number_format.t)
        : (int -> string) =
    Number_format.format_with_zeroes unit_width number_format

let number_formatter_highest (number_format: Number_format.t) : (int -> string) =
    Number_format.format_with_zeroes 0 number_format

let unit_formatter
        (number_formatter: int -> string)
        (extra: string)
        (joiner: string)
        (number: int)
        : string =
    (number_formatter number) ^ extra ^ joiner

type t_total_fn_with_args = int -> string

let total
        (index: r_index)
        (g_format: int -> string)
        (m_format: int -> string)
        (k_format: int -> string)
        (b_format: int -> string)
        (number: int)
        : string =

    let (g: int) = Threshold.division number index.g_threshold index.t_threshold in
    let (m: int) = Threshold.division number index.m_threshold index.g_threshold in
    let (k: int) = Threshold.division number index.k_threshold index.m_threshold in
    let (b: int) = Threshold.division number index.b_threshold index.k_threshold in

    let (g_s: string) = if number < index.g_threshold
        then index.g_empty
        else g_format g
    in

    let (m_s: string) = if number < index.m_threshold
        then index.m_empty
        else m_format m
    in

    let (k_s: string) = if number < index.k_threshold
        then index.k_empty
        else k_format k
    in

    let (b_s: string) = b_format b in

    index.prefix ^ g_s ^ m_s ^ k_s ^ b_s

let empty (index: r_index) : string =
    index.prefix_empty ^ index.g_empty ^ index.m_empty ^ index.m_empty ^ index.b_empty

type t_print_fn = t_total_fn_with_args -> string -> int -> unit

let print_total
        (fn_total: t_total_fn_with_args)
        (extra: string)
        (total: int)
        : unit =
    let (_: unit) = Printf.printf "%s %s" (fn_total total) extra in ()

let print_total_dummy
        (_: t_total_fn_with_args)
        (_: string)
        (_: int)
        : unit =
    ()

type t_print_fn_with_args = int -> unit
type t_print_select_fn = bool -> string -> t_print_fn_with_args

let select_print
        (fn_total: t_total_fn_with_args)
        (show_index: bool)
        : (string -> t_print_fn_with_args) =

    let (print_fn: t_print_fn) = match show_index with
    | true -> print_total
    | false -> print_total_dummy
    in
    print_fn fn_total

let print_empty
        (str_empty: string)
        (_: unit)
        : unit =
    let (_: unit) = Printf.printf "%s " str_empty in ()
