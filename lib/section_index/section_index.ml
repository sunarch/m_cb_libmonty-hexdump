(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let (default: string) =
    let (module Index: Index_module.t_index) = Multipart_index.index_module in
    Index.name

let available = [
    Multipart_index.index_module;
    Decimal_index.index_module;
    Hexadecimal_index.index_module;
    Octal_index.index_module;
]

let fn_valid (module Index: Index_module.t_index) : string =
    let index_name = Index.name in
    if index_name = default
        then index_name ^ " (default)"
        else index_name

let valid = String.concat " / " (List.map fn_valid available)

let fn_valid_long (module Index: Index_module.t_index) : string =
    Index.valid

let valid_long = String.concat " / " (List.map fn_valid_long available)

let fn_select_to_fold
        (index_format_str: string)
        (acc: (module Index_module.t_index) option)
        (index_module_item: (module Index_module.t_index))
        : (module Index_module.t_index) option =

    let (module Index_item: Index_module.t_index) = index_module_item in
    let (item_match: bool) = List.mem index_format_str Index_item.options in
    match acc with
    | Some index_module -> Some index_module
    | None -> if item_match then Some index_module_item else None

let select (index_format_str: string) : (module Index_module.t_index) =
    let fn_select_to_fold_with_init = fn_select_to_fold index_format_str in
    let (selected: (module Index_module.t_index) option) =
        List.fold_left fn_select_to_fold_with_init None available in
    match selected with
    | Some index_module -> index_module
    | None -> begin
        let (_: unit) = print_endline begin String.concat "" [
            "Error: invalid index type '";
            index_format_str;
            "' - valid values are: ";
            valid_long;
        ] end in
        exit 1
    end

type t_print_end_total_fn = int -> unit

let print_end_total (number: int) : unit =
    let (_: unit) = print_endline ("Read total bytes: " ^ (Multipart_index.fmt_total number)) in ()

let print_end_total_dummy (_: int) : unit =
    ()

let select_print_end_total (show_total: bool) : t_print_end_total_fn =
    match show_total with
    | true -> print_end_total
    | false -> print_end_total_dummy
