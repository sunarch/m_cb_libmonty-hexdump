(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let record : Index_common.r_index = {

    short = "m";
    medium = "mul";
    long = "multipart";

    valid = "";

    prefix = ""; prefix_empty = "";
    unit_width = 4;
    number_format = Number_format.Decimal;
    joiner = "";

    t_threshold = Threshold.tebi;
    g_threshold = Threshold.gibi; g_extra = "g"; g_empty = "";
    m_threshold = Threshold.mibi; m_extra = "m"; m_empty = "";
    k_threshold = Threshold.kibi; k_extra = "k"; k_empty = "";
    b_threshold = Threshold.one;  b_extra = "";  b_empty = "";
}

let record = { record with
    valid = Fmt.item_valid_3 record.short record.medium record.long;
    prefix_empty = Index_common.init_empty_prefix record.prefix;
    g_empty = Index_common.init_empty_g record;
    m_empty = Index_common.init_empty_m record;
    k_empty = Index_common.init_empty_k record;
    b_empty = Index_common.init_empty_b record;
}

let number_formatter_regular (number: int) =
    if number < 1_000
        then " " ^ Index_common.number_formatter_regular 3 record.number_format number
        else Index_common.number_formatter_regular record.unit_width record.number_format number

let (number_formatter_highest: int -> string) =
    Index_common.number_formatter_highest record.number_format

let (g_format: int -> string) =
    Index_common.unit_formatter
        number_formatter_highest record.g_extra record.joiner

let (m_format: int -> string) =
    Index_common.unit_formatter
        number_formatter_regular record.m_extra record.joiner

let (k_format: int -> string) =
    Index_common.unit_formatter
        number_formatter_regular record.k_extra record.joiner

let (b_format: int -> string) =
    Index_common.unit_formatter
        number_formatter_regular record.b_extra "" (* empty joiner *)

let (fmt_total: Index_common.t_total_fn_with_args) =
    Index_common.total
        record
        g_format m_format k_format b_format

let (empty : string) = Index_common.empty record

module Index = struct
    let (name: string) = record.long
    let (options: string list) = [record.short; record.medium; record.long;]
    let (valid: string) = record.valid
    let (select_print: Index_common.t_print_select_fn) = Index_common.select_print fmt_total
    let (print_empty: unit -> unit) = Index_common.print_empty empty
end

let index_module = (module Index : Index_module.t_index)
