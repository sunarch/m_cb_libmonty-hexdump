(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let (default: string) =
    let (module Repr: Repr_module.t_repr) = Unicode_repr.repr_module in
    Repr.name

let available = [
    Unicode_repr.repr_module;
    Dot_repr.repr_module;
    Caret_repr.repr_module;
    Backslash_repr.repr_module;
]

let fn_valid (module Repr: Repr_module.t_repr) : string =
    let repr_name = Repr.name in
    if repr_name = default
        then repr_name ^ " (default)"
        else repr_name

let valid = String.concat " / " (List.map fn_valid available)

let fn_valid_long (module Repr: Repr_module.t_repr) : string =
    Repr.valid

let valid_long = String.concat " / " (List.map fn_valid_long available)

let fn_select_to_fold
        (repr_format: string)
        (acc: (module Repr_module.t_repr) option)
        (repr_module_item: (module Repr_module.t_repr))
        : (module Repr_module.t_repr) option =

    let (module Repr_item: Repr_module.t_repr) = repr_module_item in
    let (item_match: bool) = List.mem repr_format Repr_item.options in
    match acc with
    | Some repr_module -> Some repr_module
    | None -> if item_match then Some repr_module_item else None

let select (repr_format: string) : (module Repr_module.t_repr) =
    let fn_select_to_fold_with_init = fn_select_to_fold repr_format in
    let (selected: (module Repr_module.t_repr) option) =
        List.fold_left fn_select_to_fold_with_init None available in
    match selected with
    | Some repr_module -> repr_module
    | None -> begin
        let (_: unit) = print_endline begin String.concat "" [
            "Error: invalid representation format '";
            repr_format;
            "' - valid values are: ";
            valid_long;
        ] end in
        exit 1
    end
