(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

type r_repr = {
    short : string;
    long : string;

    valid: string;
}

type t_formatter_fn_1 = char -> string
type t_formatter_fn_2 = bool -> t_formatter_fn_1

let escape_extended
    (marker: string)
    (character: char)
    : string =
    marker ^ (Ascii.code_string_with_brackets character)

let print_one
        (fn_formatter: t_formatter_fn_1)
        (character: char)
        : unit =
    let (_: unit) = print_string (fn_formatter character) in ()

type t_print_fn_2 = int -> bytes ref -> unit
type t_print_fn_4 = t_formatter_fn_1 -> string -> t_print_fn_2

let print_repr
        (fn_formatter: t_formatter_fn_1)
        (pre: string)
        (_: int)
        (buffer: bytes ref)
        : unit =

    let (_: unit) = print_string pre in
    let f element = print_one fn_formatter element in
    let (_: unit) = Bytes.iter f !buffer in
    ()

let print_repr_last_line
        (fn_formatter: t_formatter_fn_1)
        (pre: string)
        (read: int)
        (buffer: bytes ref)
        : unit =

    let (_: unit) = print_string pre in
    let (_: unit) = for i = 0 to (read - 1) do
        let (_: unit) = print_one fn_formatter (Bytes.get !buffer i) in ()
    done in
    ()

let print_repr_dummy
        (_: t_formatter_fn_1)
        (_: string)
        (_: int)
        (_: bytes ref)
        : unit =
    ()

type t_print_select_fn_3 = bool -> bool -> string -> t_print_fn_2 * t_print_fn_2
type t_print_select_fn_4 = t_formatter_fn_2 -> t_print_select_fn_3

let select_print
        (fn_formatter: t_formatter_fn_2)
        (show_repr: bool)
        (render_extended: bool)
        (pre: string)
        : t_print_fn_2 * t_print_fn_2 =

    let (fn_print: t_print_fn_4), (fn_print_last_line: t_print_fn_4) =
        match show_repr with
        | true -> print_repr, print_repr_last_line
        | false -> print_repr_dummy, print_repr_dummy
    in
    let (fn_formatter: t_formatter_fn_1) = fn_formatter render_extended in
    fn_print fn_formatter pre, fn_print_last_line fn_formatter pre
