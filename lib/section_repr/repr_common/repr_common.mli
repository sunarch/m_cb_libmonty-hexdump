(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

type r_repr = {
    short : string;
    long : string;

    valid: string;
}

val escape_extended : string -> char -> string

type t_formatter_fn_1 = char -> string
type t_formatter_fn_2 = bool -> t_formatter_fn_1

type t_print_fn_2 = int -> bytes ref -> unit
type t_print_fn_4 = t_formatter_fn_1 -> string -> t_print_fn_2

type t_print_select_fn_3 = bool -> bool -> string -> t_print_fn_2 * t_print_fn_2
type t_print_select_fn_4 = t_formatter_fn_2 -> t_print_select_fn_3
val select_print : t_print_select_fn_4
