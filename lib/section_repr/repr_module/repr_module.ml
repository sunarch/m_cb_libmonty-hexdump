(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

module type t_repr = sig
    val name : string
    val options : string list
    val valid : string
    val select_print : Repr_common.t_print_select_fn_3
end
