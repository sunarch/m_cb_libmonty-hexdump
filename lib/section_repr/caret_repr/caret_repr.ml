(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let record : Repr_common.r_repr = {
    short = "c";
    long = "caret";

    valid = "";
}

let record = { record with
    valid = Fmt.item_valid_2 record.short record.long;
}

let char_formatter
        (render_extended: bool)
        (character: char)
        : string =

    match Char.code character with
    | n when (0 <= n && n <= 31) -> Printf.sprintf "^%c" (Char.chr (64 + n))
    | 127 -> "^?"
    | n when n > 127 -> if render_extended
        then String.make 1 character
        else Repr_common.escape_extended "^" character
    | _ -> String.make 1 character

module Repr = struct
    let (name: string) = record.long
    let (options: string list) = [record.short; record.long;]
    let (valid: string) = record.valid
    let (select_print: Repr_common.t_print_select_fn_3) =
        Repr_common.select_print char_formatter
end

let repr_module = (module Repr : Repr_module.t_repr)
