(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

val program_name : string

val print_full_and_exit : unit -> unit

val print_title : string ref -> unit
