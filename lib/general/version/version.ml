(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let program_name = "hexflush"

let version_major = 0
let version_minor = 2
let version_patch = 0

let version = Printf.sprintf "%d.%d.%d" version_major version_minor version_patch

let version_full = program_name ^ " " ^ version

let print_full_and_exit (_: unit) : unit =
    let (_: unit) = print_endline version_full in
    exit 0

let print_title (input_file_name: string ref) : unit =
    let (_: unit) = print_string program_name in
    let (_: unit) = if !input_file_name <> "" then begin
        let (_: unit) = print_string " - " in
        let (_: unit) = print_string !input_file_name in
        ()
    end in
    let (_: unit) = print_endline "" in
    ()
