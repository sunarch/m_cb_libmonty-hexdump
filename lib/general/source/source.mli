(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

type t_source_fn = in_channel -> bytes ref -> int ref -> int -> int

val from_file : t_source_fn
val from_random : t_source_fn
