(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

type t_source_fn = in_channel -> bytes ref -> int ref -> int -> int

let from_file
        (ic: in_channel)
        (buffer: bytes ref)
        (pos: int ref)
        (bytes_per_line: int)
        : int =

    let (read: int) = In_channel.input ic !buffer !pos bytes_per_line in
    read

let from_random
        (_: in_channel)
        (buffer: bytes ref)
        (_: int ref)
        (bytes_per_line: int)
        : int =

    let random_gen (_: int) =
        Char.chr (Random.int Threshold.byte) in
    let (_: unit) =
        buffer := Bytes.init bytes_per_line random_gen in
    let (read: int) = bytes_per_line in
    read
