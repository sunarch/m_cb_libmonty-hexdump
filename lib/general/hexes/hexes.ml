(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let default = 16

let validate_arg_bytes_per_line (bytes_per_line: int) : unit =
    if bytes_per_line < 1 then begin
        let (_: unit) = print_endline "Error: bytes per line must be at least 1" in
        exit 1
    end

let print_one (character: char) : unit =
    let (_: unit) = Printf.printf "%02X " (Char.code character) in ()

let print (buffer: bytes ref) : unit =
    let f element = print_one element in
        Bytes.iter f !buffer

let print_last
        (bytes_per_line: int)
        (read: int)
        (buffer: bytes ref)
        : unit =

    let (_: unit) = for i = 0 to (read - 1) do
        let (_: unit) = print_one (Bytes.get !buffer i) in ()
    done in
    let (_: unit) = for _ = read to (bytes_per_line - 1) do
        let (_: unit) = Printf.printf "   " in ()
    done in
    ()

(* header ----------------------------------------------------------- *)

let print_header_values (bytes_per_line: int) : unit =
    for i = 0 to (bytes_per_line - 1) do
        let (_: unit) = print_one (Char.chr i) in ()
    done

let print_header_separator (bytes_per_line: int) : unit =
    for _ = 0 to (bytes_per_line - 1) do
        let (_: unit) = Printf.printf "-- " in ()
    done

let print_header
        (index_show: bool)
        (index_print_empty: unit -> unit)
        (bytes_per_line: int)
        : unit =

    (* Header values *)
    let (_: unit) = if index_show then begin
        let (_: unit) = index_print_empty () in
        let (_: unit) = print_string Fmt.column_separator in
        ()
    end else () in
    let (_: unit) = print_header_values bytes_per_line in
    let (_: unit) = print_endline Fmt.column_separator in
    (* Separator line *)
    let (_: unit) = if index_show then begin
        let (_: unit) = index_print_empty () in
        let (_: unit) = print_string Fmt.column_separator in
        ()
    end else () in
    let (_: unit) = print_header_separator bytes_per_line in
    let (_: unit) = print_endline Fmt.column_separator in
    ()
