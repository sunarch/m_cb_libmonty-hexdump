(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

val default: int

val validate_arg_bytes_per_line : int -> unit

val print : bytes ref -> unit
val print_last : int -> int -> bytes ref -> unit

val print_header : bool -> (unit -> unit) -> int -> unit
