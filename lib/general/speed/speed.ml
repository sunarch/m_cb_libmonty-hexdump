(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

type t_speed =
    Unset
    | Fast
    | Medium
    | Slow
    | Step

let fast_short = "f"
let fast_long = "fast"
let (fast_valid: string) = Fmt.item_valid_2 fast_short fast_long

let medium_short = "m"
let medium_long = "medium"
let (medium_valid: string) = Fmt.item_valid_2 medium_short medium_long

let slow_short = "s"
let slow_long = "slow"
let (slow_valid: string) = Fmt.item_valid_2 slow_short slow_long

let step_short = "1"
let step_long = "step"
let (step_valid: string) = Fmt.item_valid_2 step_short step_long

let unset_speed = "UNUSED"
let unset_sleep = -32.0

let speed_to_time (speed: t_speed) : float =
    match speed with
    | Unset -> 0.0
    | Fast -> 0.01
    | Medium -> 0.05
    | Slow -> 0.1
    | Step -> 0.5

let valid = String.concat " / " [
    fast_long ^ " (default)";
    medium_long;
    slow_long;
    step_long;
]

let valid_long = String.concat " / " [
    fast_valid;
    medium_valid;
    slow_valid;
    step_valid;
]

let validate_speed_sleep_overlap
        (speed: t_speed)
        (sleep_secs: float)
        : unit =
    if speed <> Unset && sleep_secs <> unset_sleep then begin
        let (_: unit) = print_endline "Error: -sleep and -speed option both provided" in
        exit 1
    end

let validate_arg_sleep (sleep_secs: float) : unit =
    if sleep_secs <> unset_sleep && sleep_secs < 0.0 then begin
        let (_: unit) = print_endline "Error: -sleep must be at least 0" in
        exit 1
    end

let select_speed (speed_str: string) : t_speed =
    match speed_str with
    | _ when speed_str = step_short || speed_str = step_long -> Step
    | _ when speed_str = slow_short || speed_str = slow_long -> Slow
    | _ when speed_str = medium_short || speed_str = medium_long -> Medium
    | _ when speed_str = fast_short || speed_str = fast_long -> Fast
    | _ when speed_str = unset_speed -> Unset
    | _ -> begin
        let (_: unit) = print_endline (String.concat "" [
            "Error: speed '";
            speed_str;
            "' not recognized, valid values are: ";
            valid_long;
        ]) in
        exit 1
    end

type t_sleep_fn_1 = unit -> unit

let sleep_time (seconds: float) (_: unit) : unit =
    Unix.sleepf seconds

let sleep_zero (_: float) (_: unit) : unit = ()

let select
        (speed_str: string)
        (sleep_secs: float)
        : t_sleep_fn_1 =

    let (speed: t_speed) = select_speed speed_str in
    let (_: unit) = validate_arg_sleep sleep_secs in
    let (_: unit) = validate_speed_sleep_overlap speed sleep_secs in
    let (time: float) = if sleep_secs <> unset_sleep
        then sleep_secs
        else speed_to_time speed
        in
    if time > 0.0
        then sleep_time time
        else sleep_zero time
