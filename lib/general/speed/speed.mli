(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

val unset_speed : string
val unset_sleep : float

val valid : string
val valid_long : string

type t_sleep_fn_1 = unit -> unit

val select : string -> float -> t_sleep_fn_1
