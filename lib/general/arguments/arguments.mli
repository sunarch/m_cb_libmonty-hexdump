(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

val arg_version : bool ref

val arg_index : string ref
val arg_bytes_per_line : int ref
val arg_repr : string ref

val arg_no_title : bool ref
val arg_no_header : bool ref
val arg_no_index : bool ref
val arg_no_repr : bool ref
val arg_no_total : bool ref
val arg_no_extended : bool ref

val arg_speed : string ref
val arg_sleep : float ref

val arg_random : bool ref

val arg_input_file : string ref

val parse_and_validate : unit -> unit
