(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let usage_msg = Version.program_name ^ " [OPTIONS] <file>"

let arg_version = ref false

let arg_index: string ref = ref Section_index.default
let arg_bytes_per_line: int ref = ref Hexes.default
let arg_repr = ref Section_repr.default

let arg_no_title = ref false
let arg_no_header = ref false
let arg_no_index = ref false
let arg_no_total = ref false
let arg_no_repr = ref false
let arg_no_extended = ref false

let arg_speed = ref Speed.unset_speed
let arg_sleep = ref Speed.unset_sleep

let arg_random = ref false

let arg_input_file = ref ""
let arg_anon_fun (filename: string) =
    let (_: unit) = if !arg_input_file <> "" then begin
        let (_: unit) = Printf.printf "Error: multiple file arguments: '%s'" filename in
        let (_: unit) = print_endline "" in
        exit 1
    end in
    arg_input_file := filename

type t_doc_prefix =
    PrefixOutput
    | PrefixSet
    | PrefixDontShow

let doc_prepare
        ?(valid: string = "")
        (prefix: t_doc_prefix)
        (text: string)
        : string =

    let alignment_separator = " " in
    let (prefix: string) = match prefix with
    | PrefixOutput -> "Output"
    | PrefixSet -> "Set"
    | PrefixDontShow -> "Don't show"
    in
    let (valid: string) = match valid with
    | "" -> ""
    | _ -> ": " ^ valid
    in
    alignment_separator ^ prefix ^ " " ^ text ^ valid

type t_spec_list = (Arg.key * Arg.spec * Arg.doc) list
let (spec_list: t_spec_list) = [
    ("-version", Arg.Set arg_version,
        (doc_prepare PrefixOutput "version information and exit"));

    ("-index", Arg.Set_string arg_index,
        doc_prepare PrefixSet "index type" ~valid:Section_index.valid);
    ("-bytes", Arg.Set_int arg_bytes_per_line,
        doc_prepare PrefixSet "bytes per line");
    ("-repr", Arg.Set_string arg_repr,
        doc_prepare PrefixSet "representation" ~valid:Section_repr.valid);

    ("-no-title", Arg.Set arg_no_title,
        doc_prepare PrefixDontShow "title at the start of output");
    ("-no-header", Arg.Set arg_no_header,
        doc_prepare PrefixDontShow "header row");
    ("-no-index", Arg.Set arg_no_index,
        doc_prepare PrefixDontShow "indexes");
    ("-no-repr", Arg.Set arg_no_repr,
        doc_prepare PrefixDontShow "representation");
    ("-no-total", Arg.Set arg_no_total,
        doc_prepare PrefixDontShow "total bytes at the end of output");
    ("-no-extended", Arg.Set arg_no_extended,
        doc_prepare PrefixDontShow "try to render extended ASCII characters in repr");

    ("-speed", Arg.Set_string arg_speed,
        doc_prepare PrefixSet "sleep time by speed value: " ~valid:Speed.valid);
    ("-sleep", Arg.Set_float arg_sleep,
        doc_prepare PrefixSet "sleep time between lines directly (seconds)");

    ("-random", Arg.Set arg_random,
        doc_prepare PrefixOutput "random data");
]

let parse (_: unit) : unit =
    let (spec_list: t_spec_list) = Arg.align spec_list in
    let (_: unit) = Arg.parse spec_list arg_anon_fun usage_msg in
    ()

let validate_arg_input_file (_: unit) : unit =
    let (_: unit) = if !arg_input_file = "" && not !arg_random then begin
    	let (_: unit) = print_endline "Error: no input file provided" in
        exit 1
    end in
    let (_: unit) = if !arg_input_file <> "" && !arg_random then begin
        let (_: unit) = print_endline "Error: input file and -random option both provided" in
        exit 1
    end in
    let (_: unit) = if not (Sys.file_exists !arg_input_file) && not !arg_random then begin
        let (_: unit) = Printf.printf "Error: input file does not exist: '%s'" !arg_input_file in
        let (_: unit) = print_endline "" in
        exit 1
    end in
    ()

let validate_all (_: unit) : unit =
    let (_: unit) = Hexes.validate_arg_bytes_per_line !arg_bytes_per_line in
    let (_: unit) = validate_arg_input_file () in
    ()

let parse_and_validate (_: unit) : unit =
    let (_: unit) = parse () in
    let (_: unit) = validate_all () in
    ()
