(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

val nul : char
val nul_unicode : string

val soh : char
val soh_unicode : string

val stx : char
val stx_unicode : string

val etx : char
val etx_unicode : string

val eot : char
val eot_unicode : string

val enq : char
val enq_unicode : string

val ack : char
val ack_unicode : string

val bel : char
val bel_unicode : string

val bs : char
val bs_unicode : string

val ht : char
val ht_unicode : string

val lf : char
val lf_unicode : string

val vt : char
val vt_unicode : string

val ff : char
val ff_unicode : string

val cr : char
val cr_unicode : string

val so : char
val so_unicode : string

val si : char
val si_unicode : string

val dle : char
val dle_unicode : string

val dc1 : char
val dc1_unicode : string

val dc2 : char
val dc2_unicode : string

val dc3 : char
val dc3_unicode : string

val dc4 : char
val dc4_unicode : string

val nak : char
val nak_unicode : string

val syn : char
val syn_unicode : string

val etb : char
val etb_unicode : string

val can : char
val can_unicode : string

val em : char
val em_unicode : string

val sub : char
val sub_unicode : string

val esc : char
val esc_unicode : string

val fs : char
val fs_unicode : string

val gs : char
val gs_unicode : string

val rs : char
val rs_unicode : string

val us : char
val us_unicode : string

val sp : char
val sp_unicode : string

val exclamation : char

val quotation : char

val hash : char

val dollar : char

val percent : char

val ampersand : char

val apostrophe : char

val parenthesis_left : char

val parenthesis_right : char

val asterisk : char

val plus : char

val comma : char

val hyphen_minus : char

val stop : char

val slash : char

val number_0 : char

val digit_1 : char

val digit_2 : char

val digit_3 : char

val digit_4 : char

val digit_5 : char

val digit_6 : char

val digit_7 : char

val digit_8 : char

val digit_9 : char

val colon : char

val semicolon : char

val lt : char

val eq : char

val gt : char

val question : char

val at : char

val a_upper : char

val b_upper : char

val c_upper : char

val d_upper : char

val e_upper : char

val f_upper : char

val g_upper : char

val h_upper : char

val i_upper : char

val j_upper : char

val k_upper : char

val l_upper : char

val m_upper : char

val n_upper : char

val o_upper : char

val p_upper : char

val q_upper : char

val r_upper : char

val s_upper : char

val t_upper : char

val u_upper : char

val v_upper : char

val w_upper : char

val x_upper : char

val y_upper : char

val z_upper : char

val bracket_left : char

val backslash : char

val bracket_right : char

val caret : char

val underscore : char

val grave_accent : char

val a_lower : char

val b_lower : char

val c_lower : char

val d_lower : char

val e_lower : char

val f_lower : char

val g_lower : char

val h_lower : char

val i_lower : char

val j_lower : char

val k_lower : char

val l_lower : char

val m_lower : char

val n_lower : char

val o_lower : char

val p_lower : char

val q_lower : char

val r_lower : char

val s_lower : char

val t_lower : char

val u_lower : char

val v_lower : char

val w_lower : char

val x_lower : char

val y_lower : char

val z_lower : char

val brace_left : char

val vertical_bar : char

val brace_right : char

val tilde : char

val del : char
val del_unicode : string

val unicode_alt : char -> string option

val code_string : string -> string -> char -> string
val code_string_with_brackets : char -> string
