(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

(* Acknowledgement:
The site https://www.asciihex.com/ascii-table was helpful in creating this module.*)

(* 000    0b 0000 0000    0o000    0x00    NUL - Null character *)
let nul = Char.chr 0
let nul_unicode = "␀"

(* 001    0b 0000 0001    0o001    0x01    SOH - Start of Heading *)
let soh = Char.chr 1
let soh_unicode = "␁"

(* 002    0b 0000 0010    0o002    0x02    STX - Start of text *)
let stx = Char.chr 2
let stx_unicode = "␂"

(* 003    0b 0000 0011    0o003    0x03    ETX - End of Text *)
let etx = Char.chr 3
let etx_unicode = "␃"

(* 004    0b 0000 0100    0o004    0x04    EOT - End of Transmission *)
let eot = Char.chr 4
let eot_unicode = "␄"

(* 005    0b 0000 0101    0o005    0x05    ENQ - Enquiry *)
let enq = Char.chr 5
let enq_unicode = "␅"

(* 006    0b 0000 0110    0o006    0x06    ACK - Acknowledge *)
let ack = Char.chr 6
let ack_unicode = "␆"

(* 007    0b 0000 0111    0o007    0x07    BEL - Bell, Alert *)
let bel = Char.chr 7
let bel_unicode = "␇"

(* 008    0b 0000 1000    0o010    0x08    BS - Backspace *)
let bs = Char.chr 8
let bs_unicode = "␈"

(* 009    0b 0000 1001    0o011    0x09    HT - Character Tabulation, Horizontal Tabulation *)
let ht = Char.chr 9
let ht_unicode = "␉"

(* 010    0b 0000 1010    0o012    0x0A    LF - Line feed *)
let lf = Char.chr 10
let lf_unicode = "␊"

(* 011    0b 0000 1011    0o013    0x0B    VT - Line Tabulation, Vertical Tabulation *)
let vt = Char.chr 11
let vt_unicode = "␋"

(* 012    0b 0000 1100    0o014    0x0C    FF - Form Feed *)
let ff = Char.chr 12
let ff_unicode = "␌"

(* 013    0b 0000 1101    0o015    0x0D    CR - Carriage Return *)
let cr = Char.chr 13
let cr_unicode = "␍"

(* 014    0b 0000 1110    0o016    0x0E    SO - Shift Out *)
let so = Char.chr 14
let so_unicode = "␎"

(* 015    0b 0000 1111    0o017    0x0F    SI - Shift In *)
let si = Char.chr 15
let si_unicode = "␏"

(* 016    0b 0001 0000    0o020    0x10    DLE - Data Link Escape *)
let dle = Char.chr 16
let dle_unicode = "␐"

(* 017    0b 0001 0001    0o021    0x11    DC1 - Device Control One (XON) *)
let dc1 = Char.chr 17
let dc1_unicode = "␑"

(* 018    0b 0001 0010    0o022    0x12    DC2 - Device Control Two *)
let dc2 = Char.chr 18
let dc2_unicode = "␒"

(* 019    0b 0001 0011    0o023    0x13    DC3 - Device Control Three (XOFF) *)
let dc3 = Char.chr 19
let dc3_unicode = "␓"

(* 020    0b 0001 0100    0o024    0x14    DC4 - Device Control Four *)
let dc4 = Char.chr 20
let dc4_unicode = "␔"

(* 021    0b 0001 0101    0o025    0x15    NAK - Negative Acknowledge *)
let nak = Char.chr 21
let nak_unicode = "␕"

(* 022    0b 0001 0110    0o026    0x16    SYN - Synchronous Idle *)
let syn = Char.chr 22
let syn_unicode = "␖"

(* 023    0b 0001 0111    0o027    0x17    ETB - End of Transmission Block *)
let etb = Char.chr 23
let etb_unicode = "␗"

(* 024    0b 0001 1000    0o030    0x18    CAN - Cancel *)
let can = Char.chr 24
let can_unicode = "␘"

(* 025    0b 0001 1001    0o031    0x19    EM - End of medium *)
let em = Char.chr 25
let em_unicode = "␙"

(* 026    0b 0001 1010    0o032    0x1A    SUB - Substitute *)
let sub = Char.chr 26
let sub_unicode = "␚"

(* 027    0b 0001 1011    0o033    0x1B    ESC - Escape *)
let esc = Char.chr 27
let esc_unicode = "␛"

(* 028    0b 0001 1100    0o034    0x1C    FS - File Separator *)
let fs = Char.chr 28
let fs_unicode = "␜"

(* 029    0b 0001 1101    0o035    0x1D    GS - Group Separator *)
let gs = Char.chr 29
let gs_unicode = "␝"

(* 030    0b 0001 1110    0o036    0x1E    RS - Record Separator *)
let rs = Char.chr 30
let rs_unicode = "␞"

(* 031    0b 0001 1111    0o037    0x1F    US - Unit Separator *)
let us = Char.chr 31
let us_unicode = "␟"

(* 032    0b 0010 0000    0o040    0x20    SP - Space *)
let sp = Char.chr 32
let sp_unicode = "␠"

(* 033    0b 0010 0001    0o041    0x21    ! - Exclamation mark (point) *)
let exclamation = Char.chr 33

(* 034    0b 0010 0010    0o042    0x22    " - Quotation mark, quotes, quotemarks "*)
let quotation = Char.chr 34

(* 035    0b 0010 0011    0o043    0x23    # - Number sign, hash *)
let hash = Char.chr 35

(* 036    0b 0010 0100    0o044    0x24    $ - Dollar sign *)
let dollar = Char.chr 36

(* 037    0b 0010 0101    0o045    0x25    % - Percent sign *)
let percent = Char.chr 37

(* 038    0b 0010 0110    0o046    0x26    & - Ampersand *)
let ampersand = Char.chr 38

(* 039    0b 0010 0111    0o047    0x27    ' - Apostrophe *)
let apostrophe = Char.chr 39

(* 040    0b 0010 1000    0o050    0x28    ( - Left parenthesis, left round bracket *)
let parenthesis_left = Char.chr 40

(* 041    0b 0010 1001    0o051    0x29    ) - Right parenthesis, right round bracket *)
let parenthesis_right = Char.chr 41

(* 042    0b 0010 1010    0o052    0x2A    * - Asterisk *)
let asterisk = Char.chr 42

(* 043    0b 0010 1011    0o053    0x2B    + - Plus sign *)
let plus = Char.chr 43

(* 044    0b 0010 1100    0o054    0x2C    , - Comma *)
let comma = Char.chr 44

(* 045    0b 0010 1101    0o055    0x2D    - - Hyphen-minus, minus sign *)
let hyphen_minus = Char.chr 45

(* 046    0b 0010 1110    0o056    0x2E    . - Full stop, full point, dot *)
let stop = Char.chr 46

(* 047    0b 0010 1111    0o057    0x2F    / - Slash *)
let slash = Char.chr 47

(* 048    0b 0011 0000    0o060    0x30    0 - Zero number, zero digit *)
let number_0 = Char.chr 48

(* 049    0b 0011 0001    0o061    0x31    1 - One number, one digit *)
let digit_1 = Char.chr 49

(* 050    0b 0011 0010    0o062    0x32    2 - Two number, two digit *)
let digit_2 = Char.chr 50

(* 051    0b 0011 0011    0o063    0x33    3 - Three number, three digit *)
let digit_3 = Char.chr 51

(* 052    0b 0011 0100    0o064    0x34    4 - Four number, four digit *)
let digit_4 = Char.chr 52

(* 053    0b 0011 0101    0o065    0x35    5 - Five number, five digit *)
let digit_5 = Char.chr 53

(* 054    0b 0011 0110    0o066    0x36    6 - Six number, six digit *)
let digit_6 = Char.chr 54

(* 055    0b 0011 0111    0o067    0x37    7 - Seven number, seven digit *)
let digit_7 = Char.chr 55

(* 056    0b 0011 1000    0o070    0x38    8 - Eight number, eight digit *)
let digit_8 = Char.chr 56

(* 057    0b 0011 1001    0o071    0x39    9 - Nine number, nine digit *)
let digit_9 = Char.chr 57

(* 058    0b 0011 1010    0o072    0x3A    : - Colon symbol, colon sign *)
let colon = Char.chr 58

(* 059    0b 0011 1011    0o073    0x3B    ; - Semicolon symbol, semicolon sign *)
let semicolon = Char.chr 59

(* 060    0b 0011 1100    0o074    0x3C    < - Less-than sign *)
let lt = Char.chr 60

(* 061    0b 0011 1101    0o075    0x3D    = - Equal symbol, equal sign *)
let eq = Char.chr 61

(* 062    0b 0011 1110    0o076    0x3E    > - Greater-than sign *)
let gt = Char.chr 62

(* 063    0b 0011 1111    0o077    0x3F    ? - Question mark (point) *)
let question = Char.chr 63

(* 064    0b 0100 0000    0o100    0x40    @ - At sign, at symbol, commercial at *)
let at = Char.chr 64

(* 065    0b 0100 0001    0o101    0x41    A - Letter A (upper, uppercase) ISO basic Latin alphabet *)
let a_upper = Char.chr 65

(* 066    0b 0100 0010    0o102    0x42    B - Letter B (upper, uppercase) ISO basic Latin alphabet *)
let b_upper = Char.chr 66

(* 067    0b 0100 0011    0o103    0x43    C - Letter C (upper, uppercase) ISO basic Latin alphabet *)
let c_upper = Char.chr 67

(* 068    0b 0100 0100    0o104    0x44    D - Letter D (upper, uppercase) ISO basic Latin alphabet *)
let d_upper = Char.chr 68

(* 069    0b 0100 0101    0o105    0x45    E - Letter E (upper, uppercase) ISO basic Latin alphabet *)
let e_upper = Char.chr 69

(* 070    0b 0100 0110    0o106    0x46    F - Letter F (upper, uppercase) ISO basic Latin alphabet *)
let f_upper = Char.chr 70

(* 071    0b 0100 0111    0o107    0x47    G - Letter G (upper, uppercase) ISO basic Latin alphabet *)
let g_upper = Char.chr 71

(* 072    0b 0100 1000    0o110    0x48    H - Letter H (upper, uppercase) ISO basic Latin alphabet *)
let h_upper = Char.chr 72

(* 073    0b 0100 1001    0o111    0x49    I - Letter I (upper, uppercase) ISO basic Latin alphabet *)
let i_upper = Char.chr 73

(* 074    0b 0100 1010    0o112    0x4A    J - Letter J (upper, uppercase) ISO basic Latin alphabet *)
let j_upper = Char.chr 74

(* 075    0b 0100 1011    0o113    0x4B    K - Letter K (upper, uppercase) ISO basic Latin alphabet *)
let k_upper = Char.chr 75

(* 076    0b 0100 1100    0o114    0x4C    L - Letter L (upper, uppercase) ISO basic Latin alphabet *)
let l_upper = Char.chr 76

(* 077    0b 0100 1101    0o115    0x4D    M - Letter M (upper, uppercase) ISO basic Latin alphabet *)
let m_upper = Char.chr 77

(* 078    0b 0100 1110    0o116    0x4E    N - Letter N (upper, uppercase) ISO basic Latin alphabet *)
let n_upper = Char.chr 78

(* 079    0b 0100 1111    0o117    0x4F    O - Letter O (upper, uppercase) ISO basic Latin alphabet *)
let o_upper = Char.chr 79

(* 080    0b 0101 0000    0o120    0x50    P - Letter P (upper, uppercase) ISO basic Latin alphabet *)
let p_upper = Char.chr 80

(* 081    0b 0101 0001    0o121    0x51    Q - Letter Q (upper, uppercase) ISO basic Latin alphabet *)
let q_upper = Char.chr 81

(* 082    0b 0101 0010    0o122    0x52    R - Letter R (upper, uppercase) ISO basic Latin alphabet *)
let r_upper = Char.chr 82

(* 083    0b 0101 0011    0o123    0x53    S - Letter S (upper, uppercase) ISO basic Latin alphabet *)
let s_upper = Char.chr 83

(* 084    0b 0101 0100    0o124    0x54    T - Letter T (upper, uppercase) ISO basic Latin alphabet *)
let t_upper = Char.chr 84

(* 085    0b 0101 0101    0o125    0x55    U - Letter U (upper, uppercase) ISO basic Latin alphabet *)
let u_upper = Char.chr 85

(* 086    0b 0101 0110    0o126    0x56    V - Letter V (upper, uppercase) ISO basic Latin alphabet *)
let v_upper = Char.chr 86

(* 087    0b 0101 0111    0o127    0x57    W - Letter W (upper, uppercase) ISO basic Latin alphabet *)
let w_upper = Char.chr 87

(* 088    0b 0101 1000    0o130    0x58    X - Letter X (upper, uppercase) ISO basic Latin alphabet *)
let x_upper = Char.chr 88

(* 089    0b 0101 1001    0o131    0x59    Y - Letter Y (upper, uppercase) ISO basic Latin alphabet *)
let y_upper = Char.chr 89

(* 090    0b 0101 1010    0o132    0x5A    Z - Letter Z (upper, uppercase) ISO basic Latin alphabet *)
let z_upper = Char.chr 90

(* 091    0b 0101 1011    0o133    0x5B    [ - Left square bracket *)
let bracket_left = Char.chr 91

(* 092    0b 0101 1100    0o134    0x5C    \ - Backslash *)
let backslash = Char.chr 92

(* 093    0b 0101 1101    0o135    0x5D    ] - Right square bracket *)
let bracket_right = Char.chr 93

(* 094    0b 0101 1110    0o136    0x5E    ^ - Caret *)
let caret = Char.chr 94

(* 095    0b 0101 1111    0o137    0x5F    _ - Underscore *)
let underscore = Char.chr 95

(* 096    0b 0110 0000    0o140    0x60    ` - Grave accent *)
let grave_accent = Char.chr 96

(* 097    0b 0110 0001    0o141    0x61    a - Letter a (lower, lowercase) ISO basic Latin alphabet *)
let a_lower = Char.chr 97

(* 098    0b 0110 0010    0o142    0x62    b - Letter b (lower, lowercase) ISO basic Latin alphabet *)
let b_lower = Char.chr 98

(* 099    0b 0110 0011    0o143    0x63    c - Letter c (lower, lowercase) ISO basic Latin alphabet *)
let c_lower = Char.chr 99

(* 100    0b 0110 0100    0o144    0x64    d - Letter d (lower, lowercase) ISO basic Latin alphabet *)
let d_lower = Char.chr 100

(* 101    0b 0110 0101    0o145    0x65    e - Letter e (lower, lowercase) ISO basic Latin alphabet *)
let e_lower = Char.chr 101

(* 102    0b 0110 0110    0o146    0x66    f - Letter f (lower, lowercase) ISO basic Latin alphabet *)
let f_lower = Char.chr 102

(* 103    0b 0110 0111    0o147    0x67    g - Letter g (lower, lowercase) ISO basic Latin alphabet *)
let g_lower = Char.chr 103

(* 104    0b 0110 1000    0o150    0x68    h - Letter h (lower, lowercase) ISO basic Latin alphabet *)
let h_lower = Char.chr 104

(* 105    0b 0110 1001    0o151    0x69    i - Letter i (lower, lowercase) ISO basic Latin alphabet *)
let i_lower = Char.chr 105

(* 106    0b 0110 1010    0o152    0x6A    j - Letter j (lower, lowercase) ISO basic Latin alphabet *)
let j_lower = Char.chr 106

(* 107    0b 0110 1011    0o153    0x6B    k - Letter k (lower, lowercase) ISO basic Latin alphabet *)
let k_lower = Char.chr 107

(* 108    0b 0110 1100    0o154    0x6C    l - Letter l (lower, lowercase) ISO basic Latin alphabet *)
let l_lower = Char.chr 108

(* 109    0b 0110 1101    0o155    0x6D    m - Letter m (lower, lowercase) ISO basic Latin alphabet *)
let m_lower = Char.chr 109

(* 110    0b 0110 1110    0o156    0x6E    n - Letter n (lower, lowercase) ISO basic Latin alphabet *)
let n_lower = Char.chr 110

(* 111    0b 0110 1111    0o157    0x6F    o - Letter o (lower, lowercase) ISO basic Latin alphabet *)
let o_lower = Char.chr 111

(* 112    0b 0111 0000    0o160    0x70    p - Letter p (lower, lowercase) ISO basic Latin alphabet *)
let p_lower = Char.chr 112

(* 113    0b 0111 0001    0o161    0x71    q - Letter q (lower, lowercase) ISO basic Latin alphabet *)
let q_lower = Char.chr 113

(* 114    0b 0111 0010    0o162    0x72    r - Letter r (lower, lowercase) ISO basic Latin alphabet *)
let r_lower = Char.chr 114

(* 115    0b 0111 0011    0o163    0x73    s - Letter s (lower, lowercase) ISO basic Latin alphabet *)
let s_lower = Char.chr 115

(* 116    0b 0111 0100    0o164    0x74    t - Letter t (lower, lowercase) ISO basic Latin alphabet *)
let t_lower = Char.chr 116

(* 117    0b 0111 0101    0o165    0x75    u - Letter u (lower, lowercase) ISO basic Latin alphabet *)
let u_lower = Char.chr 117

(* 118    0b 0111 0110    0o166    0x76    v - Letter v (lower, lowercase) ISO basic Latin alphabet *)
let v_lower = Char.chr 118

(* 119    0b 0111 0111    0o167    0x77    w - Letter w (lower, lowercase) ISO basic Latin alphabet *)
let w_lower = Char.chr 119

(* 120    0b 0111 1000    0o170    0x78    x - Letter x (lower, lowercase) ISO basic Latin alphabet *)
let x_lower = Char.chr 120

(* 121    0b 0111 1001    0o171    0x79    y - Letter y (lower, lowercase) ISO basic Latin alphabet *)
let y_lower = Char.chr 121

(* 122    0b 0111 1010    0o172    0x7A    z - Letter z (lower, lowercase) ISO basic Latin alphabet *)
let z_lower = Char.chr 122

(* 123    0b 0111 1011    0o173    0x7B    { - Left curly bracket *)
let brace_left = Char.chr 123

(* 124    0b 0111 1100    0o174    0x7C    | - Vertical bar *)
let vertical_bar = Char.chr 124

(* 125    0b 0111 1101    0o175    0x7D    } - Right curly bracket *)
let brace_right = Char.chr 125

(* 126    0b 0111 1110    0o176    0x7E    ~ - Tilde symbol, tilde sign *)
let tilde = Char.chr 126

(* 127    0b 0111 1111    0o177    0x7F    DEL - Delete character *)
let del = Char.chr 127
let del_unicode = "␡"

let unicode_alt (character: char) : string option =
    match Char.code character with
    | 0 -> Some nul_unicode
    | 1 -> Some soh_unicode
    | 2 -> Some stx_unicode
    | 3 -> Some etx_unicode
    | 4 -> Some eot_unicode
    | 5 -> Some enq_unicode
    | 6 -> Some ack_unicode
    | 7 -> Some bel_unicode
    | 8 -> Some bs_unicode
    | 9 -> Some ht_unicode
    | 10 -> Some lf_unicode
    | 11 -> Some vt_unicode
    | 12 -> Some ff_unicode
    | 13 -> Some cr_unicode
    | 14 -> Some so_unicode
    | 15 -> Some si_unicode
    | 16 -> Some dle_unicode
    | 17 -> Some dc1_unicode
    | 18 -> Some dc2_unicode
    | 19 -> Some dc3_unicode
    | 20 -> Some dc4_unicode
    | 21 -> Some nak_unicode
    | 22 -> Some syn_unicode
    | 23 -> Some etb_unicode
    | 24 -> Some can_unicode
    | 25 -> Some em_unicode
    | 26 -> Some sub_unicode
    | 27 -> Some esc_unicode
    | 28 -> Some fs_unicode
    | 29 -> Some gs_unicode
    | 30 -> Some rs_unicode
    | 31 -> Some us_unicode
    | 32 -> Some sp_unicode
    | 127 -> Some del_unicode
    | _ -> None

let code_string
    (before: string)
    (after: string)
    (character: char)
    : string =
    Printf.sprintf "%s%03d%s" before (Char.code character) after

let (code_string_with_brackets: char -> string) =
    code_string "[" "]"
