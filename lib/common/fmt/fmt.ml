(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let (column_separator: string) = "| "

let item_valid_2
        (short: string)
        (long: string)
        : string =
    long ^ " (" ^ short ^ ")"

let item_valid_3
        (short: string)
        (medium: string)
        (long: string)
        : string =
    long ^ " (" ^ medium ^ ", " ^ short ^ ")"

let item_valid_4
        (short: string)
        (medium: string)
        (long: string)
        (extra: string)
        : string =
    long ^ " (" ^ medium ^ ", " ^ short ^ ", " ^ extra ^ ")"
