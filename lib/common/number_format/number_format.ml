(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let (format_conversion_prefix: char) = '%'
let (format_conversion_prefix_str: string) = String.make 1 format_conversion_prefix
let (format_conversion_escaped: string) = String.make 2 format_conversion_prefix
let (format_formatting_prefix: char) = '@'
let (format_formatting_escaped: string) =
    (String.make 1 format_conversion_prefix) ^ (String.make 1 format_formatting_prefix)

type t =
    Decimal
    | Hexadecimal
    | Octal

let to_char (number_format: t) : char =
    match number_format with
    | Decimal -> 'd'
    | Hexadecimal -> 'x'
    | Octal -> 'o'

let to_string (number_format: t) : string =
    String.make 1 (to_char number_format)

let filler_to_string (filler: char) : string =
    match filler with
    | c when c = format_conversion_prefix -> format_conversion_escaped
    | c when c = format_formatting_prefix -> format_formatting_escaped
    | _ -> String.make 1 filler

let format
        (filler: char)
        (filler_count: int)
        (number_format: t)
        (number: int)
        : string =

    let filler_str = if filler_count > 0
        then filler_to_string filler
        else ""
    in
    let filler_count_str = if filler_count > 0
        then Int.to_string filler_count
        else ""
    in
    let number_format_str = to_string number_format in

    let format = Scanf.format_from_string (String.concat "" [
        format_conversion_prefix_str;
        filler_str;
        filler_count_str;
        number_format_str;
    ]) "%d" in

    Printf.sprintf format number

let (format_with_zeroes: int -> t -> int -> string) =
    format '0'
