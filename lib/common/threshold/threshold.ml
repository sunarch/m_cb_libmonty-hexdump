(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let one = 1

let kibi = 1_024
let mibi = kibi * kibi
let gibi = mibi * kibi
let tebi = gibi * kibi

let kilo = 1_000
let mega = kilo * kilo
let giga = mega * kilo
let tera = giga * kilo

let byte = 0x01_00
let bytes_2 = byte * byte
let bytes_3 = bytes_2 * byte
let bytes_4 = bytes_3 * byte

let octal_k = 0o1_000
let octal_m = octal_k * octal_k
let octal_g = octal_m * octal_k
let octal_t = octal_g * octal_k

let division
        (value: int)
        (threshold_lower: int)
        (threshold_upper: int)
        : int =

    if value < threshold_lower
        then 0
        else (value mod threshold_upper) / threshold_lower
