(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

val one : int

val kibi : int
val mibi : int
val gibi : int
val tebi : int

val kilo : int
val mega : int
val giga : int
val tera : int

val byte : int
val bytes_2 : int
val bytes_3 : int
val bytes_4 : int

val octal_k : int
val octal_m : int
val octal_g : int
val octal_t : int

val division : int -> int -> int -> int
