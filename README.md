# hexflush

a hex dump utility

## License

[Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/)

```
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
```

## todo

* fully integrate and then delete files of previous version
  * escape with frames
  * bytes per line based on terminal width
* double line aligned output
* lower-case hexadecimal
