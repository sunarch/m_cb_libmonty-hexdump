#!/usr/bin/bash

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# opam exec -- dune init proj hexflush

# opam switch list-available

# general version ------------------------------------------------------

# opam switch create hexflush ocaml.5.1.1
# opam env --switch=hexflush

opam install ocaml-lsp-server odoc ocamlformat utop

# half-static version --------------------------------------------------

# add this item to executable in its dune file:
# (flags (:standard -cclib -static))

# static version -------------------------------------------------------
# from https://www.systutorials.com/how-to-statically-link-ocaml-programs/

# opam switch create hexflush-static 4.11.2+musl+static+flambda
# opam install ocamlfind
# ocamlfind ocamlopt -ccopt -static -o ./_build/hexflush ./bin/main.ml

# In_channel from 5.1.1 not available
# link not working: https://v2.ocaml.org/releases/4.11/api/index.html
# see: https://v2.ocaml.org/releases/4.12/api/index.html
# could be solvable with the Unix module
