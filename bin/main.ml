(* This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. *)

let process_loop
        (ic: in_channel)
        (buffer: bytes ref)
        (pos: int ref)
        (bytes_per_line: int)
        (fn_source: Source.t_source_fn)
        (index_print: int -> unit) (total: int ref)
        (repr_print: Repr_common.t_print_fn_2)
        (repr_print_last: Repr_common.t_print_fn_2)
        : bool =

    let (read: int) = fn_source ic buffer pos bytes_per_line in
    let (is_end_of_source: bool) = read = 0 in
    if is_end_of_source then
        let (quit_loop: bool) = true in
        quit_loop
    else
        let (_: unit) = index_print !total in
        let (_: unit) = total := !total + read in
        let (is_last_line: bool) = read < bytes_per_line in
        if is_last_line then
            let (_: unit) = Hexes.print_last bytes_per_line read buffer in
            let (_: unit) = repr_print_last read buffer in
            let (quit_loop: bool) = true in
            quit_loop
        else
            let (_: unit) = Hexes.print buffer in
            let (_: unit) = repr_print  read buffer in
            let (quit_loop: bool) = false in
            quit_loop

let process_input
        (fn_source: Source.t_source_fn)
        (bytes_per_line: int)
        (fn_sleep: unit -> unit)
        (index_print: int -> unit)
        (repr_print: Repr_common.t_print_fn_2)
        (repr_print_last: Repr_common.t_print_fn_2)
        (total_print: Section_index.t_print_end_total_fn)
        (ic: in_channel)
        : unit =

    let buffer = ref (Bytes.create bytes_per_line) in
    let pos = ref 0 in
    let total = ref 0 in

    let quit_loop = ref false in
    let (_: unit) = while not !quit_loop do
        let (_: unit) = quit_loop := process_loop
            ic buffer pos bytes_per_line
            fn_source
            index_print total
            repr_print repr_print_last
        in
        let (_: unit) = print_endline "" in
        let (_: unit) = fn_sleep () in
        ()
    done in

    let (_: unit) = total_print !total in
    ()

let () =
    (* Arguments *)
    let (_: unit) = Arguments.parse_and_validate () in
    (* Arguments - special output *)
    let (show_version_only: bool) = !Arguments.arg_version in
    (* Arguments - general display *)
    let (bytes_per_line: int) = !Arguments.arg_bytes_per_line in
    let (fn_sleep: Speed.t_sleep_fn_1) = Speed.select !Arguments.arg_speed !Arguments.arg_sleep in
    (* Arguments - title & header *)
    let (show_title: bool) = not !Arguments.arg_no_title in
    let (show_header: bool) = not !Arguments.arg_no_header in
    (* Arguments - index *)
    let (index_show: bool) = not !Arguments.arg_no_index in
    let (module Index: Index_module.t_index) = Section_index.select !Arguments.arg_index in
    let (index_print: Index_common.t_print_fn_with_args) =
        Index.select_print index_show Fmt.column_separator in
    let (index_print_empty: unit -> unit) = Index.print_empty in
    (* Arguments - repr *)
    let (repr_show: bool) = not !Arguments.arg_no_repr in
    let (repr_render_extended: bool) = not !Arguments.arg_no_extended in
    let (module Repr: Repr_module.t_repr) = Section_repr.select !Arguments.arg_repr in
    let (repr_print: Repr_common.t_print_fn_2),
            (repr_print_last: Repr_common.t_print_fn_2) =
        Repr.select_print repr_show repr_render_extended Fmt.column_separator in
    (* Arguments - total *)
    let (show_total: bool) = not !Arguments.arg_no_total in
    let (total_print: Section_index.t_print_end_total_fn) =
            Section_index.select_print_end_total show_total in
    (* Arguments - source *)
    let (use_random: bool) = !Arguments.arg_random in
    let (fn_source: Source.t_source_fn) = if use_random
        then Source.from_random
        else Source.from_file
    in

    (* Special output *)
    let (_: unit) = if show_version_only then Version.print_full_and_exit () in

    (* Regular output *)
    let (_: unit) = if show_title
        then Version.print_title Arguments.arg_input_file
        else () in
    let (_: unit) = if show_header
        then Hexes.print_header index_show index_print_empty bytes_per_line
        else () in
    let process_input_w_args () = process_input
        fn_source
        bytes_per_line
        fn_sleep
        index_print
        repr_print repr_print_last
        total_print
        in
    try
        if use_random then
            let (_: unit) = process_input_w_args () In_channel.stdin in ()
        else
            let (_: unit) = In_channel.with_open_bin
                !Arguments.arg_input_file (process_input_w_args ()) in ()
    with e ->
        let (_: unit) = print_endline (Printexc.to_string e) in ()
